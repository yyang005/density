//
//  main.cpp
//  Density
//
//  Created by Anna Yang on 4/16/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "glob.h"
#include <dirent.h>
#include <sys/stat.h>
#include "DensityStatics.h"

vector<string> getFilePaths(const string& pattern){
    glob_t glob_result;
    glob(pattern.c_str(),GLOB_TILDE,NULL,&glob_result);
    vector<string> files;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        files.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return files;
}

void listdir(string name, vector<string>& inDirs, vector<string>& outDirs, string base){
    DIR *dir;
    struct dirent *entry;
    
    if (!(dir = opendir(name.c_str())))
        return;
    
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            string path = name + "/" + string(entry->d_name);
            inDirs.push_back(path);
            string pathToCreate = base + "/" + string(entry->d_name);
            outDirs.push_back(pathToCreate);
            mkdir(pathToCreate.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            
            listdir(path, inDirs, outDirs, pathToCreate);
        }
    }
    closedir(dir);
}

int main(int argc, const char * argv[]) {
    if (argc < 3){
        cout << "Please provide path to input file and path to output file!" << endl;
        exit(1);
    }
    //argv[1] = "/Users/anna.yang/Desktop/Alex_code/inSample";
    //argv[2] = "/Users/anna.yang/Desktop/Alex_code/outSample";
    
    vector<string> inDirs, outDirs;
    inDirs.push_back(argv[1]);
    outDirs.push_back(argv[2]);
    listdir(argv[1], inDirs, outDirs, argv[2]);
    
    ifstream fin("parameters.txt");
    //ifstream fin("/Users/anna.yang/Desktop/Alex_code/parameters.txt");
    string line;
    vector<int> vars;
    while (getline(fin, line))
    {
        size_t pos = line.find(" ");
        vars.push_back(stoi(line.substr(pos+1)));
    }
    int widthOfSquareToDraw = vars[0];
    int numOfHistogramBinsStat = vars[1];
    int intensityStatMin = vars[2];
    int intensityThresholdForDense = vars[3];
    
    FILE *fout;
    for (int i = 0; i < inDirs.size(); i++){
        string inFile(inDirs[i]);
        string outFile(outDirs[i]);
        vector<string> files = getFilePaths(inFile+"/*.png");
        vector<string> bmp_files = getFilePaths(inFile+"/*.bmp");
        files.insert(files.end(), bmp_files.begin(), bmp_files.end());
        for (int iFile = 0; iFile < files.size(); iFile++){
            size_t pos = files[iFile].find_last_of("/");
            string imageName = files[iFile].substr(pos+1);
            Image image;
            bool succeed = image.read(files[iFile].c_str());
            size_t dotpos = imageName.find_last_of(".");
            string textFile = outFile + "/" + imageName.substr(0,dotpos) + ".txt";
            fout = fopen(textFile.c_str(), "w");
            if (fout == NULL)
            {
                printf("\n\n fout == NULL");
                exit(1);
            }
            cout << "Image: " << imageName << endl;
            if (succeed){
                cout << "Calculate the statics..." << endl;
                DensityStatics stat(intensityStatMin, intensityThresholdForDense, numOfHistogramBinsStat, widthOfSquareToDraw);
                stat.calculateStatics(image);
                
                // Save the statics into a text file
                cout << "Saving the statics..." << endl;
                stat.saveStatics(fout);
                fclose(fout);
                
                // Draw the results
                cout << "Drawing the dense area..." << endl;
                string outPath = outFile + "/" + imageName;
                stat.drawSquareOnImage(image, outPath);
                
                cout << "Processing is done !!" << endl;
            }
            else{
                fprintf(fout, "Error in reading image file: %s", imageName.c_str());
            }
        }
    }
    
    return 0;
}
