//
//  DensityStatics.h
//  Density
//
//  Created by Anna Yang on 4/17/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#ifndef DensityStatics_h
#define DensityStatics_h

#include "image.h"
#include <vector>

using namespace std;
using namespace imago;

const int numGrayLevels = 256;
const int nGrayLevelIntensityForNonDenseArea = 40;
const int nGrayLevelIntensityForDenseArea = 140;

class DensityStatics{
    void stdevOfDenseSquaresFromCentreOfMass(float& stdev){
        int centerX, centerY;
        massOfCenterForDenseSquares(centerY, centerX);
        
        int sumSquareOfDistance = 0;
        int numDenseSquare = 0;
        for (int j = 0; j < avgIntensityForSquares.size(); j++){
            for (int i = 0; i < avgIntensityForSquares[0].size(); i++){
                if (avgIntensityForSquares[j][i] >= threshForDense){
                    numDenseSquare++;
                    int diffToCenterY = j - centerY;
                    int diffToCenterX = i - centerX;
                    sumSquareOfDistance += diffToCenterY * diffToCenterY + diffToCenterX * diffToCenterX;
                }
            }
        }
        
        stdev = sqrt((float)sumSquareOfDistance / numDenseSquare);
    }
    
    void massOfCenterForDenseSquares(int& centerY, int& centerX){
        int sumOfWeightedHeightBySquares = 0;
        int sumOfWeightedWidthBySquares = 0;
        int sumOfIntensity = 0;
        
        cout << avgIntensityForSquares.size() << endl;
        cout << avgIntensityForSquares[0].size() << endl;
        for (int j = 0; j < avgIntensityForSquares.size(); j++){
            for (int i = 0; i < avgIntensityForSquares[0].size(); i++){
                if (avgIntensityForSquares[j][i] >= threshForDense){
                    sumOfWeightedHeightBySquares += j * avgIntensityForSquares[j][i];
                    sumOfWeightedWidthBySquares += i * avgIntensityForSquares[j][i];
                    sumOfIntensity += avgIntensityForSquares[j][i];
                }
            }
        }
        centerY = sumOfWeightedHeightBySquares / sumOfIntensity;
        centerX = sumOfWeightedWidthBySquares / sumOfIntensity;
    }

    int threshForBreast;
    int threshForDense;
    int nBreastArea;
    int numberOfBins;
    int squareWidth;
    vector<vector<int>> avgIntensityForSquares;
    
public:
    DensityStatics(int threshBreast, int threshDense, int nBins, int squareSize):threshForBreast(threshBreast), threshForDense(threshDense), numberOfBins(nBins), squareWidth(squareSize){
        vector<int> h(nBins, 0);
        std::vector<int>::iterator it = hist.begin();
        hist.insert(it, h.begin(), h.end());
        
        nBreastArea = 0;
        percentageForBreast = 0;
        percentageForDenseInBreast = 0;
    }
    ~DensityStatics(){};
    
    int calculateStatics(Image& image){
        int numBreastPixels = 0;
        int numDensePixels = 0;
        
        int widthOfBin = (numGrayLevels - threshForBreast) / numberOfBins;
        if (hist.size() <= 0){
            std::cout << "Error: array for histogram has size of 0!!" << std::endl;
            return -1;
        }
        
        int widthInSquare = image.width() / squareWidth;
        int heightInSquare = image.height() / squareWidth;
        avgIntensityForSquares.resize(heightInSquare);
        
        int nStep = image.pitchInBytes() / (image.width() * sizeof(unsigned char));
        for (int j = 0; j < image.height(); j++){
            int widthSteps = image.pitchInBytes() * j;
            for (int i = 0; i < image.width(); i++){
                unsigned char intensity = image.data()[widthSteps+i*nStep];
                // Count pixels belong to breast area and dense area
                if (intensity >= threshForBreast){
                    numBreastPixels++;
                    if (intensity >= threshForDense){
                        numDensePixels++;
                    }
                    
                    // Calculate histogram for breast area
                    int indexOfBin = (intensity - threshForBreast) / widthOfBin;
                    if (indexOfBin >= numberOfBins){
                        indexOfBin = numberOfBins - 1;
                    }
                    hist[indexOfBin]++;
                }
                
                // Divide image into squares
                // Keep track of the coordinates in squares and average intensity of each square
                int rowIdx = j / squareWidth;
                int colIdx = i / squareWidth;
                if (rowIdx < heightInSquare && colIdx < widthInSquare){
                    if (avgIntensityForSquares[rowIdx].size() < (colIdx+1)){
                        avgIntensityForSquares[rowIdx].push_back(0);
                    }
                    avgIntensityForSquares[rowIdx][colIdx] += intensity;
                }
            }
        }
        
        int total = image.height() * image.width();
        nBreastArea = numBreastPixels;
        percentageForBreast = (float)numBreastPixels / total;
        percentageForDenseInBreast = (float)numDensePixels / numBreastPixels;
        
        // Calculate the average of intensity of squares
        int numPixelsForSquare = squareWidth * squareWidth;
        for (vector<vector<int>>::iterator it = avgIntensityForSquares.begin(); it != avgIntensityForSquares.end(); it++){
            for (vector<int>::iterator iit = it->begin(); iit != it->end(); iit++){
                *iit = *iit / numPixelsForSquare;
                cout << *iit << endl;
            }
        }
        
        // stdev for dense squares
        stdevOfDenseSquaresFromCentreOfMass(stdvForDenseSquare);
        
        return 0;
    }

    void saveStatics(FILE *fp){
        if (nBreastArea == 0){
            //cout << "Error: No statics available to save !!" << endl;
            return;
        }
        fprintf(fp, "\n\n nBreastArea = %d, fPercentageOfBreastAreaInImage = %E, fPercentageOfDenseAreaInBreastf = %E \n", nBreastArea, percentageForBreast, percentageForDenseInBreast);
        
        for (int i = 0; i < numberOfBins; i++){
            fprintf(fp, "\n nHistogramArr[%d] = %d, fPercentagesInHistogramArr[%d] = %E", i, hist[i], i, (float)hist[i]/nBreastArea);
        }
        
        fprintf(fp, "\n\n StDev of the dense areas:  %E", stdvForDenseSquare);
    }
    
    void drawSquareOnImage(Image& image, string fileName){
        if (avgIntensityForSquares.size() == 0){
            std::cout << "No squares to draw !!" << std::endl;
            return;
        }
        for (int j = 0; j < avgIntensityForSquares.size(); j++){
            for (int i = 0; i < avgIntensityForSquares[0].size(); i++){
                int startX, startY, endX, endY;
                startY = j * squareWidth;
                endY = startY + squareWidth;
                startX = i * squareWidth;
                endX = startX + squareWidth;
                int nStep = image.pitchInBytes() / (image.width() * sizeof(unsigned char));
                if (avgIntensityForSquares[j][i] >= threshForDense){
                    for (int row = startY; row < endY; row++){
                        int widthSteps = image.pitchInBytes() * row;
                        for (int col = startX; col < endX; col++){
                            image.data()[widthSteps+col*nStep] = nGrayLevelIntensityForDenseArea;
                            image.data()[widthSteps+col*nStep+1] = nGrayLevelIntensityForDenseArea;
                            image.data()[widthSteps+col*nStep+2] = nGrayLevelIntensityForDenseArea;
                        }
                    }
                }else{
                    for (int row = startY; row < endY; row++){
                        int widthSteps = image.pitchInBytes() * row;
                        for (int col = startX; col < endX; col++){
                            int intensity = ((image.data()[widthSteps+col*nStep] >= threshForBreast)) ? nGrayLevelIntensityForNonDenseArea : 0;
                            image.data()[widthSteps+col*nStep] = intensity;
                            image.data()[widthSteps+col*nStep+1] = intensity;
                            image.data()[widthSteps+col*nStep+2] = intensity;
                        }
                    }
                }
            }
        }
        image.write(fileName);
    }

    vector<int> hist;
    float percentageForBreast;
    float percentageForDenseInBreast;
    float stdvForDenseSquare;
};

#endif /* DensityStatics_h */
